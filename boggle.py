#!/usr/bin/env python3
import sys
assert sys.version_info >= (3,0,"Incorrect version of python. Requires 3.0 or higher")

import curses.panel
import random
import curses
import os
import time

import preGameScreens
import gameScreens

# This program is for educational purposes only.

# Used code from:
# http://stackoverflow.com/questions/746082/how-to-find-list-of-possible-words-from-a-letter-matrix-boggle-solver#750012

# References:
# https://gist.github.com/myano/1055442
# https://docs.python.org/3.4/library/curses.html
# https://docs.python.org/3/library/curses.html
# https://docs.python.org/3.3/library/curses.ascii.html
# https://gist.github.com/myano/1055442
# http://www.tuxradar.com/content/code-project-build-ncurses-ui-python
# http://hakhub.blogspot.com/2011/11/python-curses-example-tutorial.html
# http://stackoverflow.com/questions/21784625/how-to-input-a-word-in-ncurses-screen
# http://linux.die.net/man/3/nodelay
# http://docs.activestate.com/activepython/2.5/python/curses/node7.html
# https://docs.python.org/2/library/curses.html
# http://ubuntuforums.org/showthread.php?t=1626888
# http://blog.sanctum.geek.nz/putty-configuration/
# http://stackoverflow.com/questions/9485699/setupterm-could-not-find-terminal-in-python-program-using-curses
# http://unix.stackexchange.com/questions/17419/using-curses-with-linux-console
# https://docs.python.org/2/library/curses.html

def main(stdscr):
	# Create curses stdscr
	stdscr = curses.initscr()
	# Hide cursor blinking
	curses.curs_set(0)
	# Seed random
	random.seed(time.time())

	# Initialize color pairs
	curses.init_pair(1, curses.COLOR_GREEN,	curses.COLOR_BLACK)
	curses.init_pair(2, curses.COLOR_RED,	curses.COLOR_BLACK)
	curses.init_pair(3, curses.COLOR_WHITE,	curses.COLOR_BLUE)
	curses.init_pair(4, curses.COLOR_YELLOW,curses.COLOR_BLACK)
	curses.init_pair(5, curses.COLOR_WHITE,	curses.COLOR_BLACK)
	curses.init_pair(6, curses.COLOR_BLACK,	curses.COLOR_GREEN)


	# Display the initial screen with the animated boggle word
	preGameScreens.boggleAnimationScreen(stdscr)

	# Display the screen with instructions
	preGameScreens.instructionScreen(stdscr)

	# Display screen to select the number of players
	numPlayers = preGameScreens.numPlayerScreen(stdscr)

	if numPlayers == 1:
		# Display screen to select the rate the computer guesses
		cGuessTimer = preGameScreens.compGuessScreen(stdscr)
	else:
		# Set the rate of computer guess to be insurmountable
		cGuessTimer = 1000000

	# Display screen to select the game duration
	gameDuration = preGameScreens.gameLength(stdscr)

	# Display screen to select the game dictionary
	dFile = preGameScreens.dictionaryScreen(stdscr)
	# Check if the dictionary file is actually a file
	if not os.path.isfile(dFile):
		return(2)

	# Run main game loop
	valid = gameScreens.gameLoop(stdscr, cGuessTimer, gameDuration, dFile, numPlayers)

	# Display the post game screen
	gameScreens.postGame(stdscr, valid)

if __name__== "__main__":
	# setenv("TERM","xterm",1);
	if(os.environ['TERM'] != 'xterm'):
		print("Incompatible terminal emulator! Changing to xterm.")
		os.environ['TERM'] = 'xterm'
	curses.wrapper(main)
