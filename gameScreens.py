import curses
import time
import random

import Classes

def gameLoop(stdscr, cGuessTimer, gameDuration, dFile, numPlayers):
	""" Main game loop """
	maxY, maxX = stdscr.getmaxyx()

	# Create initial classes
	b = Classes.Board(dFile)
	p1 = Classes.Player()
	c1 = Classes.Player()
	valid = b.printWords().split()

	# Displays board on screen, coordinates used to place objects in relation to the board
	gridyAxis, xOffset = b.displayBoard(maxY, maxX)

	# Add labels for one player
	if numPlayers == 1:
		stdscr.addstr(0,int(maxX *(2/3)+5),"Computer",curses.color_pair(3))
		stdscr.addstr(0,int(maxX *(1/3)-18)+5,"Player 1",curses.color_pair(3))

	# Create computer word box
	cHeight = 2; cWidth = 18
	comp = curses.newwin(cHeight, cWidth, 1, int(maxX * (2/3)))
	comp.scrollok(True)
	comp.box()
	comp.refresh()

	# Create user word box
	uHeight = 2; uWidth = 18
	user = curses.newwin(uHeight, uWidth, 1, int(maxX *(1/3)-18))
	stdscr.refresh()
	user.scrollok(True)
	user.box()
	user.refresh()

	# Create user score box
	p1score = curses.newwin(5, 8, int(maxY/2)+2,int((maxX /3) + ((maxX/3 - 54) /2) ))
	p1score.addstr(2,2,str(p1.score).center(3,' '),curses.color_pair(5))
	p1score.box()
	p1score.refresh()

	# Create computer score box
	compScore = curses.newwin(5, 8, int(maxY/2)+2,int((maxX /3) + ((maxX/3 + 38) /2) ))
	compScore.addstr(2,2,str(c1.score).center(3,' '),curses.color_pair(5))
	compScore.box()
	compScore.refresh()

	# Create game timer box
	end_t = time.time() + gameDuration
	cur_t = start_t = time.time()
	gameTime = end_t - start_t +1
	mins = int((gameTime/60))
	secs = int( (gameTime%60) )
	gameTimer = curses.newwin(3, 7, gridyAxis,int((maxX /3) + ((maxX/3 - 7) /2) ))
	gameTimer.addstr(1,1, "{0:02}:{1:02}".format(mins,secs), curses.color_pair(3))
	gameTimer.box()
	gameTimer.refresh()

	# Create user input box
	height = 3; width = 20
	usrWord = curses.newwin(height, width, gridyAxis+3, xOffset)
	usrWord.box()
	usrWord.nodelay(True)

	userWords = []
	c = theString = ''
	cursor = 1
	curses.noecho()
	numWords = 0
	cNumWords = 0
	cGuess = time.time() + cGuessTimer

	# Main game loop while there's still time and words to be found
	while time.time() < end_t and valid:
		# Check if a second has elapsed to update game time display
		if time.time() - cur_t > 1:
			gameTime -= 1
			mins = int( (gameTime/60) )
			secs = int( (gameTime%60) )
			gameTimer.addstr(1,1, "{0:02}:{1:02}".format(mins,secs), curses.color_pair(3))
			gameTimer.refresh()
			cur_t = time.time()

		# Get a letter from user
		c = usrWord.getch(1,cursor)
		# Check to see if it's time for the computer to find a word
		if(time.time() > cGuess):
			if (cNumWords + cHeight)> maxY /2:
				comp.setscrreg(1,(cHeight+cNumWords)-1)
				comp.scroll(1)
			else:
				cNumWords +=1
				comp.resize(cHeight + cNumWords, cWidth)

			# Get word, remove from list, display, and reset guess timer
			compWord = random.choice(valid)
			c1.score += b.scoreWord(compWord)
			compScore.addstr(2,2,str(c1.score).center(3,' '),curses.color_pair(5))

			compScore.refresh()
			valid.remove(compWord)
			comp.addstr(cNumWords,0,compWord.center(17,' '),curses.color_pair(4))
			comp.box()
			comp.refresh()
			cGuess = time.time() + cGuessTimer
		# Continue if nothing was entered or if there's only one player
		if(c == -1 or numPlayers == 2):
			continue
		# Check word if they hit enter, space, or if it's greater than 17chars
		if str(chr(c)) == '\n' or len(theString) > 17 or str(chr(c)) == ' ' :
			if len(theString) < 3:
				continue
			if numWords + cHeight > maxY /2:
				user.setscrreg(1,(uHeight+numWords)-1)
				user.scroll(1)
			else:
				numWords += 1
				user.resize(uHeight + numWords, uWidth)
			# Place word in user guessed words box; green=valid word, red=bad word
			if theString in valid:
				user.addstr(numWords,0,theString.center(17,' '),curses.color_pair(1))
				p1.score += b.scoreWord(theString)
				p1score.addstr(2,2,str(p1.score).center(3,' '),curses.color_pair(5))
				p1score.refresh()
				valid.remove(theString)
			else:
				user.addstr(numWords,0,theString.center(17,' '),curses.color_pair(2))

			# Refresh everything
			user.box()
			user.refresh()
			usrWord.clear()
			usrWord.box()
			usrWord.refresh()
			c = theString = ''
			cursor = 1
			continue
		# Check for backspace key, removes latest char
		elif c == curses.KEY_BACKSPACE or c == 127:
			if cursor > 1:
				usrWord.move(1,cursor-1)
				usrWord.clrtoeol()
				cursor -=1
				theString = theString[:-1]
				usrWord.box()
				usrWord.refresh()
			continue
		# Concatenates inputted char to string
		usrWord.addch(1,cursor,str(chr(c)))
		cursor +=1
		theString = theString + str(chr(c))

	# Game is over, refresh all the things on the screen and re-apply labels
	stdscr.clear()
	stdscr.addstr(0,int(maxX *(2/3)+5),"Computer",curses.color_pair(3))
	stdscr.addstr(0,int(maxX *(1/3)-18)+5,"Player 1",curses.color_pair(3))
	stdscr.refresh()

	compScore.touchwin()
	compScore.refresh()
	p1score.touchwin()
	p1score.refresh()

	user.box()
	user.touchwin()
	user.refresh()

	comp.box()
	comp.touchwin()
	comp.refresh()

	# Return the un-found words to be displayed post game
	return valid

def postGame(stdscr, valid):
	""" Function displays the post game statistics and unfound words """
	maxY, maxX = stdscr.getmaxyx()

	vHeight = 2; vWidth = 18
	validWords = curses.newwin(vHeight, vWidth, 1, int(maxX *(1/3)+ ((maxX/3 - 18) /2)))
	validWords.scrollok(True)

	# Loop to be able to display the un-found words multiple times
	while True:
		vNumWords = 0
		if len(valid) == 0:
			stdscr.addstr(1,int(maxX /2)-7,"All words found",curses.color_pair(3))

		# Display of words
		for word in valid:
			# Check to scroll or just resize
			if vNumWords + vHeight > maxY/2:
				validWords.setscrreg(1,vNumWords)
				validWords.scroll(1)
			else:
				vNumWords += 1
				validWords.resize(vHeight + vNumWords, vWidth)
			validWords.addstr(vNumWords,0,word.center(17,' '))
			validWords.clrtoeol()
			validWords.box()
			validWords.refresh()
			time.sleep(.5)

		stdscr.addstr(int(maxY/2)+5,int(maxX /2)-8,"Press 'q' to quit",curses.color_pair(3))
		stdscr.addstr(int(maxY/2)+6,int(maxX /2)-13,"Press anykey to replay words",curses.color_pair(3))

		# Press q to quit, any other key will display words again
		c = stdscr.getch()
		if str(chr(c)) == 'q':
			break
		validWords.clear()
		validWords.refresh()
	curses.endwin()