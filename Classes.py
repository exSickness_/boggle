import curses
import random
import re


class Player():
	def __init__(self):
		self.score = 0


class Board():
	def __init__(self, dFile):
		# Classic boggle dice set shown on http://www.bananagrammer.com/2013/10/the-boggle-cube-redesign-and-its-effect.html
		# Chosen because curses is an original text GUI, the original boggle game only seemed right.
		dice = [['a', 'a', 'c', 'i', 'o', 't'], ['a', 'b', 'i', 'l', 't', 'y'], ['a', 'b', 'j', 'm', 'o', 'q'],
			['a', 'c', 'd', 'e', 'm', 'p'], ['a', 'c', 'e', 'l', 'r', 's'], ['a', 'd', 'e', 'n', 'v', 'z'],
			['a', 'h', 'm', 'o', 'r', 's'], ['b', 'i', 'f', 'o', 'r', 'x'], ['d', 'e', 'n', 'o', 's', 'w'],
			['d', 'k', 'n', 'o', 't', 'u'], ['e', 'e', 'f', 'h', 'i', 'y'], ['e', 'g', 'k', 'l', 'u', 'y'],
			['e', 'g', 'i', 'n', 't', 'v'], ['e', 'h', 'i', 'n', 'p', 's'], ['e', 'l', 'p', 's', 't', 'u'],
			['g', 'i', 'l', 'r', 'u', 'w']]

		# Initialize values for creating the game board
		diceNum = [num for num in range(0,16)]
		count = 0
		gameBoard = l = ''

		# Loop to randomly populate the game board from above dice
		while diceNum:
			rChoice = random.choice(diceNum)
			diceNum.remove(rChoice)
			l = dice[rChoice]
			lToAdd = random.choice(l)
			if count % 4 == 0:
				gameBoard += ' '
			gameBoard += lToAdd
			count +=1
		grid = gameBoard.split()

		self._grid = grid
		self.nRow, self.nCol = len(grid), len(grid[0])

		# This is code from:
		# http://stackoverflow.com/questions/746082/how-to-find-list-of-possible-words-from-a-letter-matrix-boggle-solver#750012

		# Alphabet holds the face value of all the dice on the game board
		self.alphabet = ''.join(set(''.join(grid)))
		if 'q' in self.alphabet:
			self.alphabet + 'u'
		# Boggleable is a compiled regex used to verify potential words
		# whose first letter is on the game board and 3letters+ long
		self.bogglable = re.compile('[' + self.alphabet + ']{3,}$', re.I).match

		# Words holds every word longer than 3 letters and first letter is on game board
		self.words = set(word.rstrip('\n') for word in open(dFile) if self.bogglable(word))

		# Prefixes holds every unique combination of starting letters in words, 3+ long
		self.prefixes = set(word[:i] for word in self.words for i in range(2, len(word)+1))


	def extending(self, prefix, path):
		# Yield the prefix if it's valid
		if prefix in self.words:
			yield (prefix, path)
		# Go through each neighbor, appending them and check if it's valid
		for (nx, ny) in self.neighbors(path[-1]):
			if (nx, ny) not in path:
				if self._grid[ny][nx] == 'q':
					prefix1 = prefix + 'qu'
				else:
					prefix1 = prefix + self._grid[ny][nx]
				if prefix1 in self.prefixes:
					# Recursively call generator with new prefix
					for result in self.extending(prefix1, path + ((nx, ny),)):
						yield result

	def neighbors(self, var1):
		""" Generator used to return letters surrounding the specified letter """
		x,y = var1
		for nx in range(max(0, x-1), min(x+2, self.nCol)):
			for ny in range(max(0, y-1), min(y+2, self.nRow)):
				yield (nx, ny)

	def solve(self):
		""" Generator to get each letter in the grid and map out what valid words can be made """
		for y, row in enumerate(self._grid):
			for x, letter in enumerate(row):
				if letter == 'q':
					letter = 'qu'
				for result in self.extending(letter, ((x, y),)):
					yield result

	def printWords(self):
		""" Returns a space delimited string containing all the valid words for the board """
		return( ' '.join(sorted(set(word for (word, path) in self.solve()))) )

	def displayBoard(self, maxY, maxX):
		""" Displays the game board """
		# Initialize values used to print out the board
		xOffset = int((maxX /3) + ((maxX/3 - 20) /2) )
		gridyAxis = 1
		gridxAxis = xOffset
		count = 1
		gridOneString = ''.join(self._grid)

		# Loop to display each letter
		for i in range(16):
			# Create pad and place letter in it
			pad = curses.newpad(3, 5)
			if gridOneString[count-1] == 'q':
				pad.addstr(1,1,"qu")
			else:
				pad.addstr(1,1,gridOneString[count-1])
			# Display letter
			pad.box()
			pad.refresh( 0,0, gridyAxis,gridxAxis, maxY,maxX)
			# Display a 4x4 game board
			if(count % 4 == 0):
				gridyAxis += 3
				gridxAxis = xOffset
			else:
				gridxAxis += 5
			count += 1

		# Return the boards y axis and x offset for use to display other windows later
		return gridyAxis, xOffset

	def scoreWord(self, word):
		wordLen = len(word)
		# Checks to return the correct score
		if wordLen == 3 or wordLen == 4:
			return(1)
		elif wordLen == 5:
			return(2)
		elif wordLen == 6:
			return(3)
		elif wordLen == 7:
			return(5)
		elif wordLen > 8:
			return(11)
		else:
			return(0)

