
import curses
import random

def boggleAnimationScreen(stdscr):
	""" Displays the initial welcome screen """
	maxY, maxX = stdscr.getmaxyx()

	# Create window to hold 'welcome to' and panelize so letters can pass through
	introScr = curses.newwin(maxY, maxX, 0, 0)
	introScr.addstr(0,0, 'Y:'+str(maxY)+' X:'+str(maxX))
	introScr.addstr(int(maxY/2-2), int(maxX/2-3), "WELCOME")
	introScr.addstr(int(maxY/2-1), int(maxX/2-1), "TO")
	wel = curses.panel.new_panel(introScr)
	introScr.refresh()
	curses.panel.update_panels()

	# List used to iterate and update each letter on board
	bList = []
	# Initialize values used to designate letter destination
	tarY = int(maxY/2)
	halfX = int(maxX/2)
	placed = False

	# Create unique windows for each boggle letter
	bw1 = curses.newwin(2,2, random.randrange(1,maxY-3),random.randrange(1,maxX-3))
	bw2 = curses.newwin(2,2, random.randrange(1,maxY-3),random.randrange(1,maxX-3))
	bw3 = curses.newwin(2,2, random.randrange(1,maxY-3),random.randrange(1,maxX-3))
	bw4 = curses.newwin(2,2, random.randrange(1,maxY-3),random.randrange(1,maxX-3))
	bw5 = curses.newwin(2,2, random.randrange(1,maxY-3),random.randrange(1,maxX-3))
	bw6 = curses.newwin(2,2, random.randrange(1,maxY-3),random.randrange(1,maxX-3))

	# Add each letter to respective window
	bw1.addstr(0,0,"B")
	bw2.addstr(0,0,'O')
	bw3.addstr(0,0,'G')
	bw4.addstr(0,0,'G')
	bw5.addstr(0,0,'L')
	bw6.addstr(0,0,'E')

	# Create unique panels for each window
	p1 = curses.panel.new_panel(bw1)
	p2 = curses.panel.new_panel(bw2)
	p3 = curses.panel.new_panel(bw3)
	p4 = curses.panel.new_panel(bw4)
	p5 = curses.panel.new_panel(bw5)
	p6 = curses.panel.new_panel(bw6)

	# Append each panel, the target coordinates, and bool if it has been placed or not
	bList.append([p1, tarY, halfX-3,placed])
	bList.append([p2, tarY, halfX-2,placed])
	bList.append([p3, tarY, halfX-1,placed])
	bList.append([p4, tarY, halfX-0,placed])
	bList.append([p5, tarY, halfX+1,placed])
	bList.append([p6, tarY, halfX+2,placed])

	curses.panel.update_panels()

	# Loop until all of the letters' placed values have been set to True
	while any(letter[3] == False for letter in bList):
		# Loop through each letter
		for let in bList:
			# Extract values from the inside list
			panel,tY,tX,placed = let
			# Reset the coordinate modifiers to 0
			yChange = xChange = 0
			# Get the current window's position
			curY, curX = panel.window().getbegyx()

			# Check if current window's Y is at it's target and change accordingly
			if curY < tY:
				yChange = 1
			elif curY > tY:
				yChange = -1
			else:
				yChange = 0

			# Check if current window's X is at it's target and change accordingly
			if curX < tX:
				xChange = 1
			elif curX > tX:
				xChange = -1
			else:
				xChange = 0

			# Check if letter is at the target destination. Set placed to True
			if curY == tY and curX == tX:
				let[3] = True

			# Move, update and refresh screen
			panel.move(curY+yChange, curX+xChange)
			curses.panel.update_panels()
			stdscr.refresh()

		curses.napms(100)

	# Display a helpful hint to move forward
	stdscr.addstr(maxY-3,  int(maxX/2-27), "Program based on the original game by Hasbro - BOGGLE")
	stdscr.addstr(maxY-2,  int(maxX/2-27), "For educational purposes only. Not intended for sale.")
	stdscr.addstr(maxY-1,  int(maxX/2-16), "Advance screens by pressing enter")
	stdscr.refresh()

	# Wait until enter is pressed
	c = 0
	while str(chr(c)) != '\n':
		c = stdscr.getch()

	# Clear screens in preperation for next display
	stdscr.clear()
	stdscr.refresh()
	introScr.clear()
	introScr.refresh()

def instructionScreen(stdscr):
	""" Displays the instructions """
	maxY, maxX = stdscr.getmaxyx()

	# Display instructions and scoring
	stdscr.addstr( 0,  1, "Instructions:")
	stdscr.addstr( 1,  1, "Scroll using the arrow keys.")
	stdscr.addstr( 2,  1, "Press 'e' to enter edit mode. (Ex. 13:20)")
	stdscr.addstr( 3,  1, "Lock in your choice by pressing enter")
	stdscr.addstr( 4,  1, "Advance screens by pressing enter")
	stdscr.addstr( 5,  1, "Scoring:")
	stdscr.addstr( 6,  1, "Length Score")
	stdscr.addstr( 7,  1, "  3      1")
	stdscr.addstr( 8,  1, "  4      1")
	stdscr.addstr( 9,  1, "  5      2")
	stdscr.addstr(10,  1, "  6      3")
	stdscr.addstr(11,  1, "  7      5")
	stdscr.addstr(12,  1, "  8+     11")
	stdscr.addstr( maxY-4,  int(maxX/2)-28, "Entering invalid custom settings will terminate the game")
	stdscr.addstr( maxY-3,  int(maxX/2)-15, "or cause undefined behavior")

	# Display a helpful hint to move forward
	stdscr.addstr(maxY-1,  int(maxX/2-16), "Advance screens by pressing enter")
	stdscr.refresh()

	# Advance when enter is pressed
	c = 0
	while str(chr(c)) != '\n':
		c = stdscr.getch()

	stdscr.clear()
	stdscr.refresh()


def numPlayerScreen(stdscr):
	""" Displays the screen to select number of players """
	maxY, maxX = stdscr.getmaxyx()

	# Create windows and default to one player
	onePlyr = curses.newwin(1, 16, int(maxY/5), int(maxX/2)-7)
	twoPlyr = curses.newwin(1, 16, int((maxY/5)*2), int(maxX/2)-7)
	numPlayers = 1

	# Add strings to respective windows
	onePlyr.addstr(0, 0, "ONE PLAYER GAME")
	twoPlyr.addstr(0, 0, "TWO PLAYER GAME")
	stdscr.addstr(maxY-1,  int(maxX/2-16), "Advance screens by pressing enter")

	c = 0
	# Loop until selection is made
	while str(chr(c)) != '\n':
		if c == curses.KEY_UP:
			numPlayers *= -1
		elif c == curses.KEY_DOWN:
			numPlayers *= -1
		else:
			pass
		# Update background of current selected choice
		if numPlayers < 0:
			onePlyr.bkgd(curses.color_pair(5))
			twoPlyr.bkgd(curses.color_pair(6))
		else:
			onePlyr.bkgd(curses.color_pair(6))
			twoPlyr.bkgd(curses.color_pair(5))

		# Refresh windows
		onePlyr.refresh()
		twoPlyr.refresh()
		stdscr.refresh()
		c = stdscr.getch()

	# Clear screen in preperation for next display
	stdscr.clear()
	stdscr.refresh()

	# Variable will be greater than zero if 1 player, less than for 2 players
	if numPlayers > 0:
		return 1
	return 2



def compGuessScreen(stdscr):
	""" Displays the screen to select rate of computer guesses """
	maxY, maxX = stdscr.getmaxyx()

	# Create unique windows for each difficulty
	hardWin = curses.newwin(1, 20, int((maxY/5)*2+0), int(maxX/2)-8)
	medWin = curses.newwin(1, 20, int((maxY/5)*2+1), int(maxX/2)-8)
	easyWin = curses.newwin(1, 20, int((maxY/5)*2+2), int(maxX/2)-8)
	begWin = curses.newwin(1, 20, int((maxY/5)*2+3), int(maxX/2)-8)
	othWin = curses.newwin(1, 20, int((maxY/5)*2+4), int(maxX/2)-8)

	# Add strings to respective windows
	stdscr.addstr(int((maxY/5)*2-2),  int(maxX/2)-9, "COMPUTER GUESS TIME")
	stdscr.addstr(maxY-1, int(maxX/2)-36, "Press 'e' to edit the custom time and enter to lock it in. Format: 00:03")
	hardWin.addstr(0, 0, "Hard - 5 secs")
	medWin.addstr(0, 0, "Medium - 7 secs")
	easyWin.addstr(0, 0, "Easy - 10 secs")
	begWin.addstr(0, 0, "Beginner - 15 secs")
	othWin.addstr(0, 0, "Other -")

	# Difficulties is a list that holds each window, used to iterate through and update them
	difficulties = []
	difficulties.append(hardWin)
	difficulties.append(medWin)
	difficulties.append(easyWin)
	difficulties.append(begWin)
	difficulties.append(othWin)

	# Used to keep track of the current selected difficulty
	numDiffs = len(difficulties)

	# Used for input validation
	oneToNine = ['0','1','2','3','4','5','6','7','8','9']

	# idx will determine which difficulty was selected
	idx = 0

	# Initialize temporary values
	cGuessTimer = 15
	c = 0
	tempChar = 1
	tempUI = ''
	tempCharOffset = 0
	# Loop until difficulty is selected
	while str(chr(c)) != '\n':
		# User pressed up arrow key, loops around if at end
		if c == curses.KEY_UP:
			if idx == 0:
				idx = numDiffs-1
			else:
				idx -=1
		# User pressed down arrow key, loops around if at end
		elif c == curses.KEY_DOWN:
			if idx == numDiffs-1:
				idx = 0
			else:
				idx +=1
		# Check if user wants to enter custom time
		elif idx == 4 and str(chr(c)) == 'e':
			# Loop until custom time is entered
			while str(chr(tempChar)) != '\n':
				tempChar = othWin.getch(0,1)
				tmpWordLen = len(tempUI)
				# Check for backspace
				if tempChar == curses.KEY_BACKSPACE or tempChar == 127:
					# If there's something to delete, delete one char
					if tempCharOffset > 0:
						othWin.move(0,8+tempCharOffset-1)
						othWin.clrtoeol()
						tempCharOffset -=1
						tempUI = tempUI[:-1]
						othWin.refresh()
					continue
				# Check for too many character input and : in the correct spot
				elif tmpWordLen > 4 or(tmpWordLen == 2 and str(chr(tempChar)) != ':'):
					continue
				# Check for numeric character, excluding the : spot
				if tmpWordLen != 2 and str(chr(tempChar)) not in oneToNine:
					continue

				# Add character to string
				othWin.addch(0,8+tempCharOffset, str(chr(tempChar)))
				tempUI += str(chr(tempChar))
				tempCharOffset +=1
				othWin.refresh()
		# Unknown key entered
		else:
			pass

		# Loop to update difficulties and highlight currently selected one
		for diff in range(0,numDiffs):
			if diff == idx:
				difficulties[diff].bkgd(curses.color_pair(6))
			else:
				difficulties[diff].bkgd(curses.color_pair(5))
			difficulties[diff].refresh()

		# Get next character
		stdscr.refresh()
		c = stdscr.getch()

	# Timer will be selected when enter was pressed
	if idx == 0:
		cGuessTimer = 5
	elif idx == 1:
		cGuessTimer = 7
	elif idx == 2:
		cGuessTimer = 10
	elif idx == 3:
		cGuessTimer = 15
	elif idx == 4:
		# Invalid input. Exit program
		if len(tempUI) == 0:
			exit(1)
		cGuessM, cGuessS = tempUI.split(':')
		cGuessTimer = int(cGuessM) * 60 + int(cGuessS)
	else:
		cGuessTimer = 30

	# Clear screen in preperation for next display
	stdscr.clear()
	stdscr.refresh()

	# Return the frequency in which the computer will guess
	return cGuessTimer


def gameLength(stdscr):
	""" Displays the screen to select game duration """
	maxY, maxX = stdscr.getmaxyx()

	# Create unique windows for each time
	time_s = curses.newwin(1, 20, int((maxY/5)*2+0), int(maxX/2)-8)
	time_m = curses.newwin(1, 20, int((maxY/5)*2+1), int(maxX/2)-8)
	time_l = curses.newwin(1, 20, int((maxY/5)*2+2), int(maxX/2)-8)
	time_o = curses.newwin(1, 20, int((maxY/5)*2+3), int(maxX/2)-8)

	# Add strings to respective windows
	stdscr.addstr(int((maxY/5)*2-2),  int(maxX/2)-4, "GAME TIME")
	stdscr.addstr(maxY-1, int(maxX/2)-36, "Press 'e' to edit the custom time and enter to lock it in. Format: 00:03")
	time_s.addstr(0, 0, "Short - 1 minute")
	time_m.addstr(0, 0, "Medium - 2 minutes")
	time_l.addstr(0, 0, "Long - 3 minutes")
	time_o.addstr(0, 0, "Other -")

	# gTime is a list that holds each window, used to iterate through and update them
	gTime = []
	gTime.append(time_s)
	gTime.append(time_m)
	gTime.append(time_l)
	gTime.append(time_o)

	# Used to keep track of the current selected time
	numTimes = len(gTime)

	# Used for input validation
	oneToNine = ['0','1','2','3','4','5','6','7','8','9']

	# idx will determine which time was selected
	idx = 0

	# Initialize temporary values
	gameDuration = 1
	c = 0
	tempChar = 0
	tempUI = ''
	tempCharOffset = 0
	# Loop until time is selected
	while str(chr(c)) != '\n':
		# User pressed up arrow key, loops around if at end
		if c == curses.KEY_UP:
			if idx == 0:
				idx = numTimes-1
			else:
				idx -=1
		# User pressed down arrow key, loops around if at end
		elif c == curses.KEY_DOWN:
			if idx == numTimes-1:
				idx = 0
			else:
				idx +=1
		# Check if user wants to enter custom time
		elif idx == 3 and str(chr(c)) == 'e':
			# Loop until custom time is entered
			while str(chr(tempChar)) != '\n':
				tempChar = time_o.getch(0,1)
				tmpWordLen = len(tempUI)
				# Check for backspace
				if tempChar == curses.KEY_BACKSPACE or tempChar == 127:
					# If there's something to delete, delete one char
					if tempCharOffset > 0:
						time_o.move(0,8+tempCharOffset-1)
						time_o.clrtoeol()
						tempCharOffset -=1
						tempUI = tempUI[:-1]
						time_o.refresh()
					continue
				# Check for too many character input and : in the correct spot
				elif tmpWordLen > 4 or(tmpWordLen == 2 and str(chr(tempChar)) != ':'):
					continue
				# Check for too many character input and : in the correct spot
				if tmpWordLen != 2 and str(chr(tempChar)) not in oneToNine:
					continue

				# Add character to string
				time_o.addch(0,8+tempCharOffset, str(chr(tempChar)))
				tempUI += str(chr(tempChar))
				tempCharOffset +=1
				time_o.refresh()
		# Unknown key entered
		else:
			pass

		# Loop to update times and highlight currently selected one
		for dur in range(0,numTimes):
			if dur == idx:
				gTime[dur].bkgd(curses.color_pair(6))
			else:
				gTime[dur].bkgd(curses.color_pair(5))
			gTime[dur].refresh()

		# Get next character
		stdscr.refresh()
		c = stdscr.getch()

	# Game duration will be selected when enter was pressed
	if idx == 0:
		gameDuration = 60
	elif idx == 1:
		gameDuration = 120
	elif idx == 2:
		gameDuration = 180
	elif idx == 3:
		# Invalid input. Exit program
		if len(tempUI) == 0:
			exit(1)
		gDurM, gDurS = tempUI.split(':')
		gameDuration = int(gDurM) * 60 + int(gDurS)
	else:
		gameDuration = 0

	# Clear screen in preperation for next display
	stdscr.clear()
	stdscr.refresh()

	# Return the game duration
	return gameDuration


def dictionaryScreen(stdscr):
	""" Displays the screen to select game dictionary """
	maxY, maxX = stdscr.getmaxyx()

	# Create unique windows for each dictionary selection
	dic_d = curses.newwin(1, 33, int((maxY/5)*2+0), int(maxX/2)-16)
	dic_o = curses.newwin(1, 33, int((maxY/5)*2+1), int(maxX/2)-16)

	# Add strings to respective windows
	stdscr.addstr(int((maxY/5)*2-2),  int(maxX/2)-9, "GAME DICTIONARY")
	stdscr.addstr(maxY-2, int(maxX/2)-24, "Dictionary format must have one word per line")
	stdscr.addstr(maxY-1, int(maxX/2)-33, "Press 'e' to edit the custom dictionary and enter to lock it in.")
	dic_d.addstr(0, 0, "Default - /usr/share/dict/words")
	dic_o.addstr(0, 0, "Other -")

	# gDic is a list that holds each window, used to iterate through and update them
	gDic = []
	gDic.append(dic_d)
	gDic.append(dic_o)

	# Used to keep track of the current selected difficulty
	numDic = len(gDic)

	# idx will determine which difficulty was selected
	idx = 0

	# Initialize temporary values
	dFile = "/usr/share/dict/words"
	c = 0
	tempChar = 0
	tempCharOffset = 0
	tmpWordLen = 0
	tempUI = ''
	# Loop until dictionary is selected
	while str(chr(c)) != '\n':
		# User pressed up arrow key, loops around if at end
		if c == curses.KEY_UP:
			if idx == 0:
				idx = numDic-1
			else:
				idx -=1
		# User pressed down arrow key, loops around if at end
		elif c == curses.KEY_DOWN:
			if idx == numDic-1:
				idx = 0
			else:
				idx +=1
		# Check if user wants to enter custom dictionary
		elif idx == 1 and str(chr(c)) == 'e':
			# Loop until custom dictionary is entered
			while str(chr(tempChar)) != '\n':
				tempChar = dic_o.getch(0,1)
				tmpWordLen = len(tempUI)
					# If there's something to delete, delete one char
				if tempChar == curses.KEY_BACKSPACE or tempChar == 127:
					if tempCharOffset > 0:
						dic_o.move(0,8+tempCharOffset-1)
						dic_o.clrtoeol()
						tempCharOffset -=1
						tempUI = tempUI[:-1]
						dic_o.refresh()
					continue
				# Check for too many characters inputted
				elif tmpWordLen > 22:
					continue
				# Break if user entered.
				if str(chr(tempChar)) == '\n':
					break

				# Add character to string
				dic_o.addch(0,8+tempCharOffset, str(chr(tempChar)))
				tempUI += str(chr(tempChar))
				tempCharOffset +=1
				dic_o.refresh()
		# Unknown key entered
		else:
			pass

		# Loop to update windows and highlight currently selected one
		for dur in range(0,numDic):
			if dur == idx:
				gDic[dur].bkgd(curses.color_pair(6))
			else:
				gDic[dur].bkgd(curses.color_pair(5))
			gDic[dur].refresh()

		# Get next character
		stdscr.refresh()
		c = stdscr.getch()

	# Dictionary will be selected when enter was pressed
	if idx == 0:
		dFile = "/usr/share/dict/words"
	elif idx == 1:
		# Invalid input. Exit program
		if len(tempUI) == 0:
			exit(1)
		dFile = tempUI
	else:
		dFile = "/usr/share/dict/words"


	# Clear screen in preperation for next display
	stdscr.clear()
	stdscr.refresh()

	# Return dictionary file to be used
	return dFile